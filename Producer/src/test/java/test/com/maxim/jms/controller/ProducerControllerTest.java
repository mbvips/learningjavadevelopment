package test.com.maxim.jms.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.maxim.jms.producer.Vendor;
import com.maxim.jms.producer.controller.ProducerController;

public class ProducerControllerTest {

	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext applicationContext;

	@Before
	public void setUp() throws Exception {

		applicationContext = new ClassPathXmlApplicationContext(
				"spring/application-config.xml");
		producerController = (ProducerController) applicationContext
				.getBean("producerController");
		vendor = new Vendor();

		vendor.setVendorName("UPS");
		vendor.setFirstName("Bob");
		vendor.setLastName("Mars");
		vendor.setAddress("789 Tri City");
		vendor.setCity("Somersworth");
		vendor.setState("New Hampshire");
		vendor.setZipCode("03878");
		vendor.setEmail("Bob.Mars@UPS.com");
		vendor.setPhoneNumber("988-999-1111");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index",producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv=producerController.processRequest(vendor,model);
		assertEquals("index",mv.getViewName());
	}

}
